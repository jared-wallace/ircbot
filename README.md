# IRCbot
IRC integration for WW

###Description
Allows you to connect an IRC channel to Watson Workspace (WW) seamlessly. All messages from WW appear in the IRC channel, and vice versa.

###Usage
1. Copy the example.cfg file to a new file named "bot.cfg".

2. Fill out the cfg file with the appropriate values.

3. Create a new self-signed cert with `openssl req -new -x509 -keyout server.pem -out server.pem -days 365 -nodes`

4. ???

5. Profit
