#!/usr/bin/python

import sys
import string
import time
import re
import logging
import threading
from logging.handlers import RotatingFileHandler
from BaseHTTPServer import HTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import ssl
import hmac
import hashlib
import socket
import struct

import ConfigParser
import base64
import json
import inspect
import requests
import irc.client
import irc.bot


class InitializationError(Exception):
    pass

class ircbot(irc.bot.SingleServerIRCBot):
    def __init__(self):
        try:
            self.readConfig()
        except Exception, e:
            # Have to shut down
            msg = ['Problem reading config file because of ']
            msg.append('{0}. Check log for details.'.format(e))
            print ''.join(msg)
            raise SystemExit

        self.appAccessKey = self.appid + ':' + self.appsecret
        self.auth = base64.b64encode(self.appAccessKey.encode('ascii'))
        # Get first auth token
        if not self.getAuthorized():
            self.log('Error getting authorized, exiting...', 'critical')
            raise SystemExit
        # Send wake up message
        msg = '*Kick the tires and light the fires* - _(IRCbot coming online)_'
        self.send_message(msg)
        t = threading.Thread(
                name='webserver',
                target=self.runserver)
        t.daemon = True
        t.start()
        irc.bot.SingleServerIRCBot.__init__(self, [(self.host, self.port)],
                self.nick, self.nick)

    def on_nicknameinuse(self, c, e):
        self.log('Nickname in use on the IRC server, adding underscore',
                'error')
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        self.log('Got welcome message from IRC', 'debug')
        c.join(self.channel)
        c.privmsg(self.master, "Ready to serve, master")
        c.privmsg(self.channel, "***Watson Workspace is online***")

    def on_privmsg(self, c, e):
        self.do_command(e, e.arguments[0])

    def on_pubmsg(self, c, e):
        self.log('PUBMSG: {0} - {1}'.format(e.source.nick, e.arguments[0]),
                'debug')
        a = e.arguments[0].split(":", 1)
        if len(a) > 1 and irc.strings.lower(a[0]) == irc.strings.lower(self.connection.get_nickname()):
            self.do_command(e, a[1].strip())
        else:
            self.send_message(e.arguments[0], self.color, e.source.nick)

    def on_action(self, c, e):
        self.log('ACTION: {0} - {1}'.format(e.source.nick, e.arguments[0]),
                'debug')
        self.send_message('*{0}* {1}'.format(e.source.nick, e.arguments[0]),
                self.color,
                bot.name)

    def on_join(self, c, e):
        self.log('JOIN: {0}'.format(e.source.nick), 'debug')
        self.send_message('{0} has joined {1}'.format(e.source.nick, bot.channel),
                self.color,
                bot.name)

    def on_part(self, c, e):
        self.log('PART: {0}'.format(e.source.nick), 'debug')
        self.send_message('{0} has left {1} - {2}'.format(e.source.nick, bot.channel, e.arguments[0]),
                self.color,
                bot.name)

    def do_command(self, e, cmd):
        nick = e.source.nick
        c = self.connection
        self.log('PRIVMSG: {0} - {1}'.format(nick, e.arguments[0]), 'debug')
        if cmd == "disconnect":
            self.disconnect()
        elif cmd == "die":
            self.die()
        elif cmd == "stats":
            for chname, chobj in self.channels.items():
                c.notice(nick, "--- Channel statistics ---")
                c.notice(nick, "Channel: " + chname)
                users = sorted(chobj.users())
                c.notice(nick, "Users: " + ", ".join(users))
                opers = sorted(chobj.opers())
                c.notice(nick, "Opers: " + ", ".join(opers))
                voiced = sorted(chobj.voiced())
                c.notice(nick, "Voiced: " + ", ".join(voiced))
        else:
            c.notice(nick, "Not understood: " + cmd)

    def runserver(self):
        httpd = ThreadedHTTPServer((self.webserver_host, self.webserver_port), Handler)
        httpd.socket = ssl.wrap_socket(httpd.socket,
                certfile=self.certfile,
                server_side=True)
        httpd.serve_forever()

    def getAuthorized(self):
        try:
            payload = {'grant_type': 'client_credentials'}
            resp = requests.post(
                url='https://api.watsonwork.ibm.com/oauth/token',
                headers={
                    'Authorization': 'Basic %s' % self.auth.decode('ascii'),
                    'content-type': 'application/x-www-form-urlencoded'},
                data=payload)
            self.log(
                'Response to auth token request: {0}'.format(resp.text),
                'debug')
            data = json.loads(resp.text)
            self.token = data['access_token']
            expires = data['expires_in']
            # With a 12 hour timeout, we shouldn't have to re-auth all
            # that often. Setting this allows us to forgo error checking
            # for expired auth, and reduces the times we hit IBM
            self.token_expires = time.time() + expires
            msg = ['Token: {0} expires at '.format(self.token)]
            msg.append('{0}'.format(self.token_expires))
            self.log(
                ''.join(msg),
                'debug')
            return True
        except Exception, e:
            msg = ['Could not get authorized. Network issue, or perhaps the']
            msg.append(' appid or appsecret is incorrect.')
            msg.append('Error message was {0}'.format(e))
            self.log(
                ''.join(msg),
                'debug')
            return False

    def log(self, message, level):
        '''This allows us to see what function we were in when we got called
        '''
        func = inspect.currentframe().f_back.f_code
        numeric_level = getattr(logging, level.upper(), None)
        self.logger.log(numeric_level, '%s: %s in %s:%i' % (
            message,
            func.co_name,
            func.co_filename,
            func.co_firstlineno
        ))

    def init_log(self):
        '''Set our log printing format
        '''
        numeric_level = getattr(logging, self.level.upper(), None)
        msg = ['[%(asctime)s]:  [%(levelname)s] ']
        msg.append('(%(threadName)-10s) %(message)s')

        self.logger = logging.getLogger("Travis")
        self.logger.setLevel(numeric_level)
        handler = RotatingFileHandler(
            self.logfile,
            maxBytes=500000,
            backupCount=20)
        formatter = logging.Formatter(''.join(msg))
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def send_message(self, message, color='#000000', actor=''):
        '''General purpose function. Sends what ever string is passed in
        to the appropriate destination.
        '''
        if self.debug:
            # Avoids spamming the space when testing
            print message
        else:
            # Allow two seconds for safety's sake
            if self.token_expires < (time.time() - 2):
                self.getAuthorized()
            try:
                if not actor:
                    actor = self.name
                messagePayload = {
                    'type': 'appMessage',
                    'version': 1.0,
                    'annotations': [{
                        'type': 'generic',
                        'color': color,
                        'version': 1.0,
                        'text': message,
                        'actor': {
                            'name': actor}}]}
                msg = 'Sent off {0} to space'.format(messagePayload)
                self.log(msg, 'debug')
                url = ['https://api.watsonwork.ibm.com/v1/spaces/']
                url.append(self.spaceid)
                url.append('/messages')
                headers = {
                        'Authorization': 'Bearer {0}'.format(self.token),
                        'content-type': 'application/json'}
                respMessage = requests.post(
                    url=''.join(url),
                    headers=headers,
                    json=messagePayload)
                try:
                    # Did we get json back?
                    msg = 'Response from IBM was: {0}'.format(respMessage.json)
                    self.log(msg, 'debug')
                except ValueError, e:
                    # Let's default to text then
                    msg = 'Response from IBM was: {0}'.format(respMessage.text)
                    self.log(msg, 'debug')
                finally:
                    # Regardless, get the numerical response code
                    msg = 'Status was: {0}'.format(respMessage.status_code)
                    self.log(msg, 'debug')
            except Exception as e:
                self.log('Error sending message:{0}'.format(e), 'error')

    def readConfig(self):
        '''We start off printing to console, as logging is not yet functional
        '''
        print 'Entered readConfig'
        self.config_file = 'bot.cfg'
        config = ConfigParser.ConfigParser()
        config.readfp(open(self.config_file))

        # set log level and format
        try:
            self.level = config.get('global', 'log_level')
            self.logfile = config.get('global', 'logfile')
            print 'Using logfile {0}'.format(self.logfile)
        except Exception, e:
            print 'Using default logfiles because {0}'.format(e)
            self.level = 'info'
            self.logfile = 'logs/bot.log'
        self.init_log()
        self.log('\n\nIRCbot is starting up!\n\n', 'info')
        self.log('Logging is now enabled', 'info')
        self.log('Log level is: {0}'.format(self.level), 'info')
        try:
            # Get global settings
            self.appid = config.get('global', 'appid')
            self.appsecret = config.get('global', 'appsecret')
            self.spaceid = config.get('global', 'spaceid')
            self.webhook_key = config.get('global', 'webhook_key')
            self.api_url = config.get('global', 'api_url')
            self.color = config.get('global', 'color')
            self.name = config.get('global', 'name')
            debug = config.get('global', 'debug')
            if debug.lower() == 'yes' or debug.lower() == 'true':
                self.debug = True
            else:
                self.debug = False
            self.host = config.get('irc', 'host')
            self.port = int(config.get('irc', 'port'))
            self.nick = config.get('irc', 'nick')
            self.master = config.get('irc', 'master')
            self.channel = config.get('irc', 'channel')
            self.certfile = config.get('webserver', 'certfile')
            self.webserver_host = config.get('webserver', 'host')
            self.webserver_port = int(config.get('webserver', 'port'))
        except IOError, e:
            self.log(
                    'Failed to open config file because: {0}'.format(e),
                    'error')
            raise InitializationError()
        except ConfigParser.NoSectionError, e:
            self.log(
                'Failed to locate section in config file: {0}'.format(e),
                'error')
            raise InitializationError()
        except ConfigParser.NoOptionError, e:
            self.log(
                'Failed to locate option {0} in config file'.format(e),
                'error')
            raise InitializationError()
        except Exception, e:
            self.log('Unexpected error {0}'.format(e), 'error')
            raise InitializationError()


class Handler(BaseHTTPRequestHandler):
    """
    """
    def forbidden(self):
        bot.log('reached the forbidden method', 'debug')
        self.send_response(401)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<html><head><title>GTFO</title></head>")
        self.wfile.write("<body><p>Go away.</p>")
        self.wfile.write("</body></html>")

    def do_POST(self):
        bot.log('Received a POST call', 'debug')
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        # We get json from IBM, so we have to explicitly load it to get a python object
        data = json.loads(self.data_string)

        # Log the body
        bot.log('Json loaded data is : {0}'.format(data), 'debug')
        # We only accept a post request that has a 'type' key
        if 'type' not in data:
            self.forbidden()
        # We handle auth requests differently
        if data['type'] == 'verification':
            challenge = data['challenge']
            bot.log('challenge is: {0} of type: {1}'.format(challenge, type(challenge)), 'debug')
            # need to re-encode the body as json before hashing
            body = json.dumps({"response": challenge})
            # This specific format is required by IBM
            hash_val = hmac.new(bot.webhook_key, body, hashlib.sha256).hexdigest()

            # Write our response
            self.send_response(200)
            self.send_header('X-OUTBOUND-TOKEN', hash_val)
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()
            self.wfile.write(body)
            return
        else:
            # This means we got something from the space - nessage, annotation, etc
            self.send_response(200)
            if data['content']:
                msg = data['content']
            else:
                msg_id = data['messageId']
                body = ["{ message (id: \""]
                body.append("{0}\")".format(msg_id))
                body.append("{ createdBy { displayName id }")
                body.append(" created content contentType annotations } }")
                dat = self.make_graphql_call(''.join(body))
                response = dat['data']['message']
                annotations = [json.loads(x) for x in response['annotations']]
                for note in annotations:
                    if note['type'] == 'generic':
                        msg = note['text']
            author = data['userName']
            if author == bot.name:
                return
            message = "{0}: ".format(author) + msg
            while len(message) > 400:
                bot.connection.privmsg(bot.channel, ' '.join(message[:400].splitlines()))
                message = "{0}: ".format(author) + message[400:]
            # Have to remove CR for IRC
            bot.connection.privmsg(bot.channel, ' '.join(message.splitlines()))

    def do_GET(self):
        self.forbidden()

    def make_graphql_call(self, body):
        if bot.token_expires < (time.time() - 2):
            bot.getAuthorized()
        url = bot.api_url + '/graphql'
        headers = {
            "Content-Type": "application/graphql",
            "x-graphql-view": "PUBLIC",
            "jwt": bot.token}
        try:
            resp = requests.post(url=url, headers=headers, data=body)
        except Exception, e:
            bot.log('Failed to make request because {0}'.format(e), 'debug')
        if resp.status_code == 200:
            data = resp.json()
            bot.log("Got GraphQL response {0}".format(data), 'debug')
            return data
        else:
            bot.log('Failed graphql call', 'error')
            bot.log('Status code was {0}'.format(resp.status_code), 'error')
            bot.log('Response text was {0}'.format(resp.text), 'error')
            return None

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """
    Handle requests in a different thread
    """


bot = ircbot()
def main():
    try:
        bot.start()
    except KeyboardInterrupt:
        msg = '*Rage against the dying of the light* - _IRCbot shutting down_'
        bot.send_message(msg)
        bot.log(
            'Caught keyboard interrupt, shutting down',
            'info')
        raise SystemExit


if __name__ == '__main__':
    main()
